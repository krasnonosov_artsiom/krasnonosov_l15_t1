package com.example.krasnonosov_l15_t1

import android.graphics.Interpolator
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.adapters.ViewPagerArticlesFragmentAdapter
import com.example.adapters.ViewPagerOnBoardingFragmentAdapter
import com.example.fragments.ArticlesFragment
import com.example.fragments.OnBoardingFragment
import com.example.utils.Descriptions
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.articles_fragment.*

class MainActivity : AppCompatActivity() {

    private val itemsOnBoarding = ArrayList<OnBoardingFragment>()
    private val itemsArticles = ArrayList<ArticlesFragment>()
    val duration: Long = 500
    private var flag = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        itemsOnBoarding.add(OnBoardingFragment("Learn", R.drawable.learn))
        itemsOnBoarding.add(OnBoardingFragment("Explore", R.drawable.explore))
        itemsOnBoarding.add(OnBoardingFragment("Multiply", R.drawable.multiply))
        itemsOnBoarding.add(OnBoardingFragment("", R.drawable.best_practice))
        itemsArticles.add(ArticlesFragment(R.drawable.photo1, "Ireland", "04.02.2019", Descriptions.IRELAND.description))
        itemsArticles.add(ArticlesFragment(R.drawable.photo2, "Caribbean", "04.02.2019", Descriptions.CARIBBEAN.description))
        itemsArticles.add(ArticlesFragment(R.drawable.photo3, "Canada", "04.02.2019", Descriptions.CANADA.description))

        setSupportActionBar(toolbar)
        toolbar.visibility = GONE
        inflateByOnBoarding()

    }

    fun changeFragment(view: View) {

        if (!flag) {
            inflateByOnBoarding()
            button.text = resources.getString(R.string.next_fragment)
        } else {
            inflateByArticles()
            button.text = resources.getString(R.string.previous_fragment)
        }

    }

    private fun inflateByArticles() {
        viewPager.adapter = ViewPagerArticlesFragmentAdapter(itemsArticles)
        flag = !flag
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if (position == 0) {
                    button.animate().alpha(1f).duration = duration
                    button.isClickable = true
                } else {
                    button.animate().alpha(0f).duration = duration
                    button.isClickable = false

                }
            }
        })
        dotsIndicator.setViewPager2(viewPager)
        toolbar.visibility = VISIBLE
    }

    private fun inflateByOnBoarding() {
        viewPager.adapter = ViewPagerOnBoardingFragmentAdapter(itemsOnBoarding)
        flag = !flag
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if(position == itemsOnBoarding.size - 1) {
                    button.animate().alpha(1f).duration = duration
                    button.isClickable = true
                } else {
                    button.animate().alpha(0f).duration = duration
                    button.isClickable = false
                }
            }
        })
        dotsIndicator.setViewPager2(viewPager)
        toolbar.visibility = GONE
    }
}
