package com.example.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.krasnonosov_l15_t1.R
import kotlinx.android.synthetic.main.on_boarding_fragment.*

class OnBoardingFragment(var title: String, var image: Int) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        titleTextView.text = title
        mainImage.setImageResource(image)
        return inflater.inflate(R.layout.on_boarding_fragment, container, false)
    }

}