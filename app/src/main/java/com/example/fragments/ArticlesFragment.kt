package com.example.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.krasnonosov_l15_t1.R
import kotlinx.android.synthetic.main.articles_fragment.*

class ArticlesFragment(var image: Int, var title: String, var lastRead: String, var description: String) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        topImageView.setImageResource(image)
        titleTextView2.text = title
        lastReadTextView.text = lastRead
        descriptionTextView.text = description
        return inflater.inflate(R.layout.articles_fragment, container, false)
    }
}