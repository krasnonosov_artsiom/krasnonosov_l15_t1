package com.example.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.fragments.OnBoardingFragment
import com.example.krasnonosov_l15_t1.R

class ViewPagerOnBoardingFragmentAdapter(var items: ArrayList<OnBoardingFragment>) :
    RecyclerView.Adapter<ViewPagerOnBoardingFragmentAdapter.ViewPagerItem1Holder>() {

    class ViewPagerItem1Holder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPagerItem1Holder =
        ViewPagerItem1Holder(LayoutInflater.from(parent.context).inflate(R.layout.on_boarding_fragment, parent, false)
        )

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewPagerItem1Holder, position: Int) {
        holder.itemView.findViewById<TextView>(R.id.titleTextView).text = items[position].title
        holder.itemView.findViewById<ImageView>(R.id.mainImage)
            .setImageResource(items[position].image)
    }
}