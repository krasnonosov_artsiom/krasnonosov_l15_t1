package com.example.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.fragments.ArticlesFragment
import com.example.krasnonosov_l15_t1.R

class ViewPagerArticlesFragmentAdapter(var items: ArrayList<ArticlesFragment>) :
    RecyclerView.Adapter<ViewPagerArticlesFragmentAdapter.ViewPagerItem2Holder>() {

    class ViewPagerItem2Holder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPagerItem2Holder =
        ViewPagerItem2Holder(LayoutInflater.from(parent.context).inflate(R.layout.articles_fragment, parent, false))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewPagerItem2Holder, position: Int) {
        holder.itemView.findViewById<ImageView>(R.id.topImageView).setImageResource(items[position].image)
        holder.itemView.findViewById<TextView>(R.id.titleTextView2).text = items[position].title
        holder.itemView.findViewById<TextView>(R.id.lastReadTextView).text = items[position].lastRead
        holder.itemView.findViewById<TextView>(R.id.descriptionTextView).text = items[position].description
    }
}